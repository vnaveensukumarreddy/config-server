package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Customer;

public interface CustomerDao extends JpaRepository<Customer, Integer> {

	Optional<Customer> findByCustomerEmail(String customerEmail);
}
