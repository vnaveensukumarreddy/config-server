package com.example.demo.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Authorities;
import com.example.demo.model.Customer;

public interface AuthoritiesDao extends JpaRepository<Authorities, Integer>{

	Set<Authorities> findByCustomer(Customer customer);
}
