package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "customer")
public class Customer {

	@Id
	private Integer customerId;
	
	private String customerName;
	
	@Column(unique = true)
	private String customerEmail;
	
	private String customerPassword;
	
	private Integer Enabled;
	

}
