package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dto.UserDto;
import com.example.demo.securityconfig.CustomUsernamePasswordAuthenticationFilter;

@Controller
public class SecurityController {
  
	@GetMapping("/sayHello")
	@ResponseBody
	public ResponseEntity<String> sayHello()
	{
		return ResponseEntity.ok("Hi Naveen");
	}
	@GetMapping("/sayHello1")
	@ResponseBody
	public ResponseEntity<String> sayHello1()
	{
		return ResponseEntity.ok("Hi Naveen1");	
	}
	
	@PostMapping("/firstLogin")
	@ResponseBody
	public ResponseEntity<String> firstLogin(@RequestBody UserDto useDto,HttpSession httpSession,HttpServletRequest httpServletRequest)
	{
		httpServletRequest.setAttribute(CustomUsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY,useDto.getCustomerEmail());
		httpServletRequest.setAttribute(CustomUsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, useDto.getCustomerPassword());
			return ResponseEntity.ok("Login Successfull");

	}
	
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();

					model.addObject("msg", 
			"You do not have permission to access this page!");
		

		model.setViewName("Access_Denied");
		return model;

	}
	  
	
	
	@GetMapping("/login")
	public ModelAndView loginPage(@RequestParam(value = "error",required = false) String error,
	@RequestParam(value = "logout",	required = false) String logout) {
		System.out.println("error"+error+ "logout"+logout);
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid Credentials provided.");
			model.setViewName("login");
			return model;
		}
  
		else if (logout != null) {
			model.addObject("message", "Logged out successfully.");
			model.setViewName("login");
			return model;
		}
		
			return new ModelAndView("login");
		
	}
  
	
	@GetMapping("/login1")
	public String login1()
	{
		return "login1";
	}
	
}
