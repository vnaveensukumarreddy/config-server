package com.example.demo.securityconfig;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import com.example.demo.model.Authorities;
import com.example.demo.model.Customer;
import com.example.demo.repository.AuthoritiesDao;
import com.example.demo.repository.CustomerDao;
@Service
public class CustomUserDetailService implements UserDetailsService {

	@Autowired
	private CustomerDao customerDao;
	@Autowired
	private AuthoritiesDao authoritiesDao;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	
		System.out.println(username);
		Customer customer=customerDao.findByCustomerEmail(username).orElseThrow(()->new UsernameNotFoundException("user not registered"));
		System.out.println(customer);
		Set<GrantedAuthority> grantedAuthorities= new HashSet<GrantedAuthority>();
		Set<Authorities> authorities=authoritiesDao.findByCustomer(customer);
		authorities.forEach((role)->
		{
			SimpleGrantedAuthority simpleGrantedAuthority=new SimpleGrantedAuthority(role.getRoleName());
			grantedAuthorities.add(simpleGrantedAuthority);
		});
		return new CustomUserDetails(customer.getCustomerEmail(), customer.getCustomerPassword(), grantedAuthorities);
	}

}
