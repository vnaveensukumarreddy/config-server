package com.ams.aeroplaneservice.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "aeroplane1000")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Aeroplane implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long aeroplaneId;
	private String aeroplaneName;
	private String aeroplaneCompanyName;
	private Long hangerId;

}
