package com.ams.aeroplaneservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ams.aeroplaneservice.dto.AeroplaneDto;
import com.ams.aeroplaneservice.service.AeroplaneService;

@RestController
@RequestMapping("/aeroplane")
public class AeroplaneController {

	@Autowired
	AeroplaneService aeroplaneService;
  
	@PostMapping("/addAeroplane")
	public ResponseEntity<String> addAeroplane(@RequestHeader("first-request") String firstRequest,@RequestBody AeroplaneDto aeroplaneDto){
		System.out.println(firstRequest);
		return ResponseEntity.ok(aeroplaneService.saveAeroplane(aeroplaneDto));
	}
	
	@GetMapping("/{aeroplaneId}/{hangerId}")
	public ResponseEntity<String> assignHanger(@PathVariable("aeroplaneId") Long aeroplaneId,@PathVariable("hangerId") Long hangerId)
	{
		return ResponseEntity.ok(aeroplaneService.assignHanger(aeroplaneId, hangerId));
	}
	
}
