package com.ams.aeroplaneservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AeroplaneDto {
	private String aeroplaneName;
	private String aeroplaneCompanyName;
}
