package com.ams.aeroplaneservice.feign;

import java.util.Optional;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ams.aeroplaneservice.model.Hanger;

@FeignClient("HangerService")
@RequestMapping("/hanger")
public interface HangerServiceFeignClient {

	@GetMapping("/{hangerId}")
	public Optional<Hanger> findHanger(@PathVariable("hangerId") Long hangerId);
}
