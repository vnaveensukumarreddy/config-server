package com.ams.aeroplaneservice.service;

import java.util.Optional;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ams.aeroplaneservice.dto.AeroplaneDto;
import com.ams.aeroplaneservice.feign.HangerServiceFeignClient;
import com.ams.aeroplaneservice.model.Aeroplane;
import com.ams.aeroplaneservice.model.Hanger;
import com.ams.aeroplaneservice.repository.AeroplaneRepository;
import com.fasterxml.jackson.databind.ObjectMapper;


@Service
public class AeroplaneService {

	@Autowired
	AeroplaneRepository aeroplaneRepository;
	
	@Autowired
	HangerServiceFeignClient hangerServiceFeignClient;
	
	@Autowired
	CircuitBreakerFactory circuitBreakerFactory;
	
	@Autowired
    ObjectMapper objectMapper;
	public String saveAeroplane(AeroplaneDto aeroplaneDto) {
		
		Aeroplane aeroplane=objectMapper.convertValue(aeroplaneDto, Aeroplane.class);
		aeroplaneRepository.save(aeroplane);
	
		return "Aeroplane Added Successfully";
	}
   
	public String assignHanger(Long aeroplaneId,Long hangerId)
	{
		CircuitBreaker circuitBreaker=null;
		circuitBreaker=circuitBreakerFactory.create("hangerService");
		Optional<Hanger> hanger=circuitBreaker.run(()->
		{
		    return hangerServiceFeignClient.findHanger(hangerId);
		},throwable->{
			//throw new RuntimeException("communication failed");
			return getDefaultHanger();
		});
		
		
		if(hanger.isPresent() && hanger.get().getHangerAvailability()>0)
		{
			
			aeroplaneRepository.assignHanger(aeroplaneId,hangerId);
		}
		return "hanger assigned successfully to"+aeroplaneId;
	}
	
	public Optional<Hanger> getDefaultHanger()
	{
	
		return Optional.of(new Hanger());
	}
     
}
