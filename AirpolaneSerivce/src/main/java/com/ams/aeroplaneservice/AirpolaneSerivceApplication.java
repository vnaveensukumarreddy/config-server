package com.ams.aeroplaneservice;

import java.time.Duration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JCircuitBreakerFactory;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JConfigBuilder;
import org.springframework.cloud.client.circuitbreaker.Customizer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.timelimiter.TimeLimiterConfig;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class AirpolaneSerivceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AirpolaneSerivceApplication.class, args);
		
		
	}
	
	@Bean
	public Customizer<Resilience4JCircuitBreakerFactory> customizeCircuitBreakerFactory()
	{
		return new Customizer<Resilience4JCircuitBreakerFactory>() {

			@Override
			public void customize(Resilience4JCircuitBreakerFactory tocustomize) {
			CircuitBreakerConfig circuitBreakerConfig=CircuitBreakerConfig.custom().failureRateThreshold(3).waitDurationInOpenState(Duration.ofSeconds(2)).build();
		    TimeLimiterConfig timeLimiterConfig=TimeLimiterConfig.custom().timeoutDuration(Duration.ofSeconds(10)).build();
		    tocustomize.configureDefault(id-> new Resilience4JConfigBuilder(id).circuitBreakerConfig(circuitBreakerConfig).timeLimiterConfig(timeLimiterConfig).build());
			}
			
		};
	}

}
