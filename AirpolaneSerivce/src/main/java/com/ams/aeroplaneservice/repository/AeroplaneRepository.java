package com.ams.aeroplaneservice.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ams.aeroplaneservice.model.Aeroplane;

public interface AeroplaneRepository extends JpaRepository<Aeroplane, Long> {

	@Transactional
	@Modifying
	@Query("update Aeroplane a set a.hangerId=:hangerId where a.aeroplaneId=:aeroplaneId")
	void assignHanger(@Param("aeroplaneId") Long aeroplaneId,@Param("hangerId") Long hangerId);

}
