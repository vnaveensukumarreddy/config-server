package com.hackathon.petpeers.repository;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.hackathon.petpeers.model.Pet;
import com.hackathon.petpeers.model.User;

public interface PetDao extends PagingAndSortingRepository<Pet, Long> {

	@Query("select  p from  Pet as p where p.owner is  null")
	public Page<Pet> getAllUnsoldPets(Pageable pageable);

	public Page<Pet> findAllByOwner(User user,Pageable pageable);
}
