package com.hackathon.petpeers.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.hackathon.petpeers.model.User;

public interface UserDao extends CrudRepository<User, Long> {

	public Optional<User> findByUserEmail(String userEmail);
	
	public User findByUserEmailAndUserPassword(String userEmail,String Password);
}
