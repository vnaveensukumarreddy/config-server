package com.hackathon.petpeers.model;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginDto {
    @NotEmpty(message = "customer id should not be null")
	private String userEmail;
    @NotEmpty(message = "password should not be null")
	private String userPassword;
    
}
