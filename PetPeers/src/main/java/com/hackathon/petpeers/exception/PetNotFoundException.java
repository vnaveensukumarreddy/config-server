package com.hackathon.petpeers.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)


@NoArgsConstructor
public class PetNotFoundException extends RuntimeException {

	
    public PetNotFoundException(String exception) {
        super(exception);
    }
}
