package com.hackathon.petpeers.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Pageable;

import com.hackathon.petpeers.dto.BuyPetDto;
import com.hackathon.petpeers.dto.PetDto;
import com.hackathon.petpeers.model.Pet;

public interface PetService {

	public PetDto savePet(PetDto petDto);

	

	public List<Pet> getAllUnSoldPets(Pageable pageable);



	public List<Pet> getMyPets(Long userId, Pageable pageable);



	public PetDto buyPet(@Valid BuyPetDto buyPetDto);

}
