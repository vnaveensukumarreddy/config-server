package com.hackathon.petpeers.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackathon.petpeers.dto.BuyPetDto;
import com.hackathon.petpeers.dto.PetDto;
import com.hackathon.petpeers.exception.UserNotFoundException;
import com.hackathon.petpeers.model.Pet;
import com.hackathon.petpeers.model.User;
import com.hackathon.petpeers.repository.PetDao;
import com.hackathon.petpeers.repository.UserDao;

@Service
public class PetServiceImpl implements PetService {

	@Autowired
	PetDao petDao;

	@Autowired
	UserDao userDao;

	@Autowired
	ObjectMapper objectMapper;

	@Override
	public PetDto savePet(PetDto petDto) {
		Pet pet = objectMapper.convertValue(petDto, Pet.class);
        //Pet pet=new Pet(0, petDto.getPetName(), petDto.getPetAge(), petDto.getPetPlace(), petDto.getPetPrice(), null);
        
      //  Pet pet1=petDao.save(pet);
		return objectMapper.convertValue(petDao.save(pet), PetDto.class);
        //return new PetDto(pet1.getPetName(), pet1.getPetAge(), pet1.getPetPlace(),pet1.getPetPrice());

	}

	@Override
	public List<Pet> getAllUnSoldPets(Pageable pageable) {
		Page<Pet> favouriteAccounts = petDao.getAllUnsoldPets(pageable); // repo.findAll(pageable)
		if (favouriteAccounts.hasContent()) {
			return favouriteAccounts.getContent();
		}
		return new ArrayList<Pet>();

	}

	@Override
	public List<Pet> getMyPets(Long userId, Pageable pageable) {

		User user = userDao.findById(userId).orElseThrow(() -> new UserNotFoundException("user not found"));
		Page<Pet> pets = petDao.findAllByOwner(user, pageable);
		if (pets.hasContent()) {
			return pets.getContent();
		}
		return new ArrayList<Pet>();
	}

	@Override
	public PetDto buyPet(BuyPetDto buyPetDto) {
		User user = userDao.findById(buyPetDto.getUserId())
				.orElseThrow(() -> new UserNotFoundException("user not found"));
		Pet pet = petDao.findById(buyPetDto.getPetId()).orElseThrow(() -> new UserNotFoundException("pet not found"));

		pet.setOwner(user);

		return objectMapper.convertValue(petDao.save(pet), PetDto.class);
	}

}
