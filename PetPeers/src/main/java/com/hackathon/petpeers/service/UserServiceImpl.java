package com.hackathon.petpeers.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackathon.petpeers.dto.UserDto;
import com.hackathon.petpeers.exception.UserNotFoundException;
import com.hackathon.petpeers.model.LoginDto;
import com.hackathon.petpeers.model.User;
import com.hackathon.petpeers.repository.UserDao;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userDao;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Override
	public UserDto saveUser(UserDto userDto) {
		
		User user = objectMapper.convertValue(userDto, User.class);
		
		return objectMapper.convertValue(userDao.save(user), UserDto.class);
	}
	
	@Override
	public String checkLogin(LoginDto loginDto) {
		return userDao.findByUserEmail(loginDto.getUserEmail()).map((user)->{
			if(user.getUserPassword().equals(loginDto.getUserPassword()))
			{
				return "Login Successfull";
			}
			return "Incorrect userName or password";
			
		}).orElseThrow(()->new UserNotFoundException("User not registered"));
		
	}

}
