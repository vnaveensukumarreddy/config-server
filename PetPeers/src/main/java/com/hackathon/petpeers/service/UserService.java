package com.hackathon.petpeers.service;

import javax.validation.Valid;

import com.hackathon.petpeers.dto.UserDto;
import com.hackathon.petpeers.model.LoginDto;

public interface UserService {

	public UserDto saveUser(UserDto userDto);

	public String checkLogin(LoginDto loginDto);

}
