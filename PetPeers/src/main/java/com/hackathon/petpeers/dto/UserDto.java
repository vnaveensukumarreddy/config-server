package com.hackathon.petpeers.dto;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

	@NotEmpty
	private String userName;
	@Email
	@Column(unique = true)
	private String userEmail;
	@Size(min = 8)
	@NotEmpty
	private String userPassword;
	private Long userMobileNumber;
}
