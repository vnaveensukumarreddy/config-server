package com.hackathon.petpeers.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BuyPetDto {

	private Long userId;
	private Long petId;
}
