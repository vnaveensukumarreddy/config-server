package com.hackathon.petpeers.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PetDto {
	@NotEmpty(message = "pet name should not be empty")
	private String petName;
	@NotNull(message = "pet page should not be null")
	private int petAge;
	@NotEmpty(message = "pet place should not be empty")
	private String petPlace;
	@NotNull(message = "pet price should not be null")
	private Integer petPrice;
}
