package com.hackathon.petpeers.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hackathon.petpeers.dto.UserDto;
import com.hackathon.petpeers.model.LoginDto;
import com.hackathon.petpeers.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
	UserService userService;
    
	private static final Logger log = LoggerFactory.getLogger(UserController.class);

	@PostMapping
	public ResponseEntity<UserDto> addUser(@Valid @RequestBody UserDto userDto) {
		log.info(" user registration");
			return new ResponseEntity<>(userService.saveUser(userDto), HttpStatus.CREATED);
	}
	@PostMapping("/login") 
	public ResponseEntity<String> customerLogin(@RequestBody LoginDto loginDto) {
		log.info("customer login");
			return new ResponseEntity<>(userService.checkLogin(loginDto), HttpStatus.OK);
		
	}
}
