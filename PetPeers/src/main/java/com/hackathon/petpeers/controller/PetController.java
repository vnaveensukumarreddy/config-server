package com.hackathon.petpeers.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.petpeers.dto.BuyPetDto;
import com.hackathon.petpeers.dto.PetDto;
import com.hackathon.petpeers.model.Pet;
import com.hackathon.petpeers.service.PetService;

import lombok.extern.slf4j.Slf4j;


@RestController
@RequestMapping("/pets")
@Slf4j
public class PetController {

	@Autowired
	PetService petService;
	
	
	@PostMapping("/addPet")
	public ResponseEntity<PetDto> addPet(@Valid @RequestBody  PetDto petDto) {
		log.info(" registration");
			return new ResponseEntity<>(petService.savePet(petDto), HttpStatus.CREATED);
	}
	
	
	@GetMapping
	public ResponseEntity<List<Pet>> getAllUnsoldPets(
			 @RequestParam(required = false,defaultValue = "0",value = "pageId") Integer pageId,
			 @RequestParam(required = false,defaultValue = "4",value = "pageSize") Integer pageSize,
			@RequestParam(required = false,defaultValue = "petAge",value = "columnName") String columnName) {
		log.info("list of all favourite account details");
		
			 Pageable pageable=PageRequest.of(pageId, pageSize,Sort.by(columnName).ascending());
			List<Pet> pets=petService.getAllUnSoldPets(pageable);
	
		return new ResponseEntity<>(pets, HttpStatus.OK);
	}
	
	@GetMapping("/{userId}")
	public ResponseEntity<List<Pet>> getMyPets(@PathVariable("userId") Long userId,
			 @RequestParam(required = false,defaultValue = "0",value = "pageId") Integer pageId,
			 @RequestParam(required = false,defaultValue = "4",value = "pageSize") Integer pageSize,
			@RequestParam(required = false,defaultValue = "petAge",value = "columnName") String columnName) {
		log.info("list of all favourite account details");
		
			 Pageable pageable=PageRequest.of(pageId, pageSize,Sort.by(columnName).ascending());
			List<Pet> pets=petService.getMyPets(userId,pageable);
	
		return new ResponseEntity<>(pets, HttpStatus.OK);
	}
	@PutMapping("/buyPet")
	public ResponseEntity<PetDto> buyPet(@Valid @RequestBody  BuyPetDto buyPetDto) {
		log.info(" registration");
			return new ResponseEntity<>(petService.buyPet(buyPetDto), HttpStatus.OK);
	}
	
}
