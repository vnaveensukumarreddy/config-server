package com.hackathon.petpeers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackathon.petpeers.controller.UserController;
import com.hackathon.petpeers.dto.UserDto;
import com.hackathon.petpeers.exception.UserNotFoundException;
import com.hackathon.petpeers.model.LoginDto;
import com.hackathon.petpeers.service.UserService;

//@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
//@AutoConfigureMockMvc
//@TestPropertySource("classpath:application-test.properties")
@WebMvcTest(controllers = UserController.class)
public class TestUserController {

	@Autowired
	private MockMvc mockMvc;
	@MockBean
	private UserService userService;
    @Autowired
	ObjectMapper objectMapper;
	@Test
	@DisplayName("User SAVE TEST")
	@Order(1)
	public void addUserTest() throws Exception {

		UserDto userDto=new UserDto("ram","sukumaqreddy@gmail.com","Rsmu@123",952410483l);
		
		
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/users")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(userDto));

		MvcResult result = mockMvc.perform(request).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.CREATED.value(), response.getStatus());
	}

	@Test
	@DisplayName(" User Login")
	@Order(2)
	public void checkLoginTest() throws Exception {
		
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/users/login")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{ \"userEmail\":\"sukumaqreddy@gmail.com\", \"userPassword\": \"Rsmu123\"}");

		
		MvcResult result = mockMvc.perform(request).andReturn();

	
		MockHttpServletResponse response = result.getResponse();

		
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		//assertEquals(MediaType.APPLICATION_JSON_VALUE, response.getContentType());

	}
	
	@Test
	@DisplayName(" User Login")
	@Order(3)
	public void checkUserTest() throws Exception {
		
		LoginDto loginDto=new LoginDto("vanveen@gmail.com", "Naveen123");
		
		when(userService.checkLogin(loginDto)).thenThrow(new UserNotFoundException("user not registeres"));
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/users/login")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(loginDto));

		
		MvcResult result = mockMvc.perform(request).andReturn();

	
		MockHttpServletResponse response = result.getResponse();

		
		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
		//assertEquals(MediaType.APPLICATION_JSON_VALUE, response.getContentType());

	}
	
}
