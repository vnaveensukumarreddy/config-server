package com.hackathon.petpeers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackathon.petpeers.controller.PetController;
import com.hackathon.petpeers.dto.BuyPetDto;
import com.hackathon.petpeers.dto.PetDto;
import com.hackathon.petpeers.exception.PetNotFoundException;
import com.hackathon.petpeers.exception.UserNotFoundException;
import com.hackathon.petpeers.service.PetService;

//@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
//@AutoConfigureMockMvc
//@TestPropertySource("classpath:application-test.properties")
@WebMvcTest(controllers = PetController.class)
public class TestPetController {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	PetService petService;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Test
	@DisplayName("PET SAVE TEST")
	@Order(1)
	public void addPetTest() throws Exception {

		
		PetDto petDto=new PetDto("DOG", 10, "HYF", 100);
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/pets/addPet")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(petDto));

		MvcResult result = mockMvc.perform(request).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.CREATED.value(), response.getStatus());
	}

	@Test
	@DisplayName(" Buy Pet Test")
	@Order(2)
	public void buyPetTest() throws Exception {
		
		BuyPetDto buyPetDto=new BuyPetDto(1l, 1l);
		
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.put("/pets/buyPet")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(buyPetDto));

		
		MvcResult result = mockMvc.perform(request).andReturn();

	
		MockHttpServletResponse response = result.getResponse();

		
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		//assertEquals(MediaType.APPLICATION_JSON_VALUE, response.getContentType());

	}
	
	@Test
	@DisplayName(" Unsold pet test")
	@Order(3)
	public void getUnsoldPetTest() throws Exception {
		
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/pets");
				
		
		MvcResult result = mockMvc.perform(request).andReturn();

	
		MockHttpServletResponse response = result.getResponse();

		
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		//assertEquals(MediaType.APPLICATION_JSON_VALUE, response.getContentType());

	}
	@Test
	@DisplayName(" my pet test")
	@Order(3)
	public void getMyPetTest() throws Exception {
		
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/pets/1");
				
		
		MvcResult result = mockMvc.perform(request).andReturn();

	
		MockHttpServletResponse response = result.getResponse();

		
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		//assertEquals(MediaType.APPLICATION_JSON_VALUE, response.getContentType());

	}
	
	@Test
	@DisplayName(" user not found Test")
	@Order(4)
	public void shouldreturn404WhenUserNotFound() throws Exception {
		
		BuyPetDto buyPetDto=new BuyPetDto(3l, 1l);
		when(petService.buyPet(buyPetDto)).thenThrow(new UserNotFoundException("user id not found"));
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.put("/pets/buyPet")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(buyPetDto));

		
		MvcResult result = mockMvc.perform(request).andReturn();

	
		MockHttpServletResponse response = result.getResponse();

		
		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
		//assertEquals(MediaType.APPLICATION_JSON_VALUE, response.getContentType());

	}
	
	@Test
	@DisplayName(" pet not found Test")
	@Order(5)
	public void shouldreturn404WhenPetNotFound() throws Exception {
		
		BuyPetDto buyPetDto=new BuyPetDto(3l, 5l);
		when(petService.buyPet(buyPetDto)).thenThrow(new PetNotFoundException("pet id not found"));
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.put("/pets/buyPet")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(buyPetDto));

		
		MvcResult result = mockMvc.perform(request).andReturn();

	
		MockHttpServletResponse response = result.getResponse();

		
		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
		//assertEquals(MediaType.APPLICATION_JSON_VALUE, response.getContentType());

	}
	
}
