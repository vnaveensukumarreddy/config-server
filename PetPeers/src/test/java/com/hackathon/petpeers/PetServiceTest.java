package com.hackathon.petpeers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackathon.petpeers.dto.PetDto;
import com.hackathon.petpeers.model.Pet;
import com.hackathon.petpeers.model.User;
import com.hackathon.petpeers.repository.PetDao;
import com.hackathon.petpeers.repository.UserDao;
import com.hackathon.petpeers.service.PetService;
import com.hackathon.petpeers.service.PetServiceImpl;

@ExtendWith(MockitoExtension.class)
public class PetServiceTest {
   @Mock
   private PetDao petDao;
	@Mock
    private UserDao userDao;
	@InjectMocks
	private PetServiceImpl petService;
	@Mock
	private ObjectMapper objectMapper;
//	@Autowired
//     @BeforeEach
//	public void init()
//	{
//		MockitoAnnotations.initMocks(this);
//	}
	@Test
	public void myPet()
	{
		User user=new User(1l,"Naveen","vnaveen@gmail.com","Naveen123",8555824671l);
		Pet pet=new Pet(1l, "DOG", 10, "HYD", 100, user);
		 List<Pet> pets = new ArrayList<>();
	        pets.add(pet);
	        Page<Pet> petPage = new PageImpl<>(pets);
	        Pageable pageable = PageRequest.of(0, 1);
	        when(userDao.findById(1l)).thenReturn(Optional.of(user));
		when(petDao.findAllByOwner(user, pageable)).thenReturn(petPage);
		 List<Pet> pets1=petService.getMyPets(1l, pageable);
		 assertEquals(pets.size(), pets1.size());
//	
	}
	
	@Test
	public void testSavePet()
	{
		PetDto petDto=new PetDto("Dog", 12, "hyd", 100);
		Pet pet=new Pet(1l, "Dog", 12, "hyd", 100, null);
		when(objectMapper.convertValue(petDto, Pet.class)).thenReturn(pet);
	    
		when(petDao.save(pet)).thenReturn(pet);
		when(objectMapper.convertValue(pet, PetDto.class)).thenReturn(petDto);
		 PetDto petDto1=petService.savePet(petDto);
		 
		 assertThat(petDto1).isNotNull();
		 assertEquals(petDto.getPetName(), petDto1.getPetName());
		 
	
	}
	
	
}
