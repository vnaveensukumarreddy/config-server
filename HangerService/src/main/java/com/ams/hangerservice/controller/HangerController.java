package com.ams.hangerservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ams.hangerservice.dto.HangerDto;
import com.ams.hangerservice.model.Hanger;
import com.ams.hangerservice.service.HangerService;

@RestController
@RequestMapping("/hanger")
public class HangerController {

	@Autowired
	HangerService hangerService;
	
	@PostMapping("/addHanger")
	public ResponseEntity<String> addHanger(@RequestBody HangerDto hangerDto)
	{
		return ResponseEntity.ok(hangerService.saveHanger(hangerDto));
	}
	
	@GetMapping("/{hangerId}")
	public ResponseEntity<Optional<Hanger>> findHanger(@PathVariable("hangerId") Long hangerId)
	{
		return ResponseEntity.ok(hangerService.findHanger(hangerId));
	}
	
	
}
