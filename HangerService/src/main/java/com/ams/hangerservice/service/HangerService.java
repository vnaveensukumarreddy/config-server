package com.ams.hangerservice.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ams.hangerservice.dto.HangerDto;
import com.ams.hangerservice.model.Hanger;
import com.ams.hangerservice.repository.HangerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class HangerService {

	@Autowired
	HangerRepository hangerRepository;
	
	@Autowired
	ObjectMapper objectMapper;
	
	
	public String saveHanger(HangerDto hangerDto)
	{
		Hanger hanger=objectMapper.convertValue(hangerDto, Hanger.class);
		hangerRepository.save(hanger);
		return "hanger saved successfully";
	}
	
	public Optional<Hanger> findHanger(Long hangerId)
	{
		return hangerRepository.findById(hangerId);
	}
}
