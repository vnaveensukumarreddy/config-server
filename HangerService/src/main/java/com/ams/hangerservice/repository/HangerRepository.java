package com.ams.hangerservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ams.hangerservice.model.Hanger;

public interface HangerRepository extends JpaRepository<Hanger, Long> {

}
