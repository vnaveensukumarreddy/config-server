package com.ams.hangerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class HangerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HangerServiceApplication.class, args);
	}

}
