package com.ams.hangerservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HangerDto {

	private String hangerName;
	private int hangerCapacity;
	private int hangerAvailability;
}
